#!/usr/bin/env python3

import urllib.request
import sys
import time
import pyperclip

#print(pyperclip.paste())

typ = sys.getfilesystemencoding()

def translate(querystr, to_l="zh", from_l="en"):
    '''
    for google tranlate by doom
    '''
    C_agent = {
	'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.165063 Safari/537.36 AppEngine-Google."
    }
    flag = 'class="t0">'
    tarurl = "https://translate.google.cn/m?hl=%s&sl=%s&q=%s \
        " % (to_l, from_l, querystr.replace(" ", "+"))
    request = urllib.request.Request(tarurl, headers=C_agent)
    page = str(urllib.request.urlopen(request).read().decode(typ))
    target = page[page.find(flag) + len(flag):]
    target = target.split("<")[0]
    return target

def remove_unused_char(string):
    string = string.replace("\r", " ")
    string = string.replace("\n", " ")
    string = string.replace("\t", " ")
    string = string.replace("‐" , " ")
    return string

def input_multiline():
    string = ""
    time_start = time.time()
    for line in sys.stdin:
       string += line
       if line == '\n':
           break
    return string

if __name__ == "__main__":
    string = input_multiline()
    string = remove_unused_char(string)
    print(translate(string))
