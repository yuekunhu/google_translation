#! /bin/bash

xsel -o -b   > temp.txt
cat  temp.txt
echo -e "\n"
echo -e "\n" >> temp.txt
./translate.py < temp.txt > result.txt
if [ $? -ne 0 ]; then
    echo -e "google translate failed.\n"
else
    echo -e "google translate success.\n"
    cat result.txt | xsel -i -b
    cat result.txt
fi
rm -f temp.txt result.txt
